## AVB Tool For Compile Android 12+

## Clone repo

```
git clone https://gitlab.com/Edwin0305x/avb_tool -b main out/host/linux-x86/bin
```

## Set permission

```
sudo chmod +rwx out/host/linux-x86/bin/avbtool
```

## Or Using 

```
make avbtool
```

